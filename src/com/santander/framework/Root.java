package com.santander.framework;

public interface Root {
    boolean isRooted(String androidTags);
}
