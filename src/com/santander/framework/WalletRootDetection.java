package com.santander.framework;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

public class WalletRootDetection implements Root{
    private static final String BUSYBOX_EXTENSION = "busybox";

    private static boolean checkRootMethod1(String androidTags) {
        // get build info
        return (androidTags != null && androidTags.contains("test-keys")) ? true : false;
    }

    private static boolean checkRootMethod2() {
        String[] paths = {"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su",
                "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (String path : paths) {
            if (new File(path).exists()) return true;
        }
        return false;
    }

    private static boolean checkRootMethod3() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(new String[]{"/system/xbin/which", "su", "id | grep root"});
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            return in.readLine() != null;
        } catch (Throwable t) {
            return false;
        } finally {
            if (process != null) process.destroy();
        }
    }

    private static boolean isPhoneRooted() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec("su");
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (process != null) {
                try {
                    process.destroy();
                } catch (Exception e) {
                }
            }
        }
    }

    private static boolean checkBusyBoxBinary() {
        String[] suPaths = {
                "/data/local/",
                "/data/local/bin/",
                "/data/local/xbin/",
                "/sbin/",
                "/su/bin/",
                "/system/bin/",
                "/system/bin/.ext/",
                "/system/bin/failsafe/",
                "/system/sd/xbin/",
                "/system/usr/we-need-root/",
                "/system/xbin/"};

        for (String path : suPaths) {
            String completePath = path.concat(BUSYBOX_EXTENSION);
            File f = new File(completePath);
            if (f.exists()) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    @Override
    public boolean isRooted(String androidTags) {
        return checkRootMethod1(androidTags) || checkRootMethod2() || checkRootMethod3() || isPhoneRooted() || checkBusyBoxBinary();
    }
}
