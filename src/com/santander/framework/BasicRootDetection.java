package com.santander.framework;

import java.io.File;

public class BasicRootDetection implements Root{
    public boolean isRooted(String androidTags) {

        // get build info
        String buildTags = androidTags;
        if (buildTags != null && buildTags.contains("test-keys")) {
            return true;
        }

        // check if Superuser.apk is present
        try {
            File file = new File("/mnt/sdcard/root/Superuser.apk");
            if (file.exists()) {
                return true;
            }
        } catch (Exception e1) {
            // ignore
        }

        return false;
    }
}
